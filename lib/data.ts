//a dummy data for the feed which consits of the user's name, the user's profile picture, the post image, the post description, the number of likes, the number of comments, and the number of shares
export const postData = [
  {
    name: 'John Doe',
    profilePic: 'https://randomuser.me/api/portraits',
    postImage: 'https://source.unsplash.com/random',
    title:'This is a post title',
    description: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.',
    likes: 123,
    comments: 12,
    shares: 44,
    },

    {
    name: 'Jane Doe',
    profilePic: 'https://randomuser.me/api/portraits',
    postImage: 'https://source.unsplash.com/random',
    title:'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.',
    description: 'This is a post description',
    likes: 123,
    comments: 12,
    shares: 44,
    },

    {
    name: 'John Doe',
    profilePic: 'https://randomuser.me/api/portraits',
    postImage: 'https://source.unsplash.com/random',
    title:'This is a post title',
    description: 'This is a post description',
    likes: 123,
    comments: 12,
    shares: 44,
    },

    {
    name: 'Jane Doe',
    profilePic: 'https://randomuser.me/api/portraits',
    postImage: 'https://source.unsplash.com/random',
    title:'This is a post title',
    description: 'This is a post description',
    likes: 123,
    comments: 12,
    shares: 44,
    },
]