/** @type {import('next').NextConfig} */
const nextConfig = {
    images: {
        domains: ['randomuser.me' , 'source.unsplash.com', 'avatars.githubusercontent.com'],
    },
};

export default nextConfig;
